// create a new scene named "Game"
let gameScene = new Phaser.Scene('Game');

//variables
var path;
var curve;
var curveB;
var elip;
var graphics;
let pointred = 0;
let pointblue = 0;
let advantageblue = "AD";
let advantagered = "AD";
let setred = 0;
let setblue = 0;
var ball;
var player;
var player2;
var playerred;
var playerblue;
var velocityX = 120 //Phaser.Math.Between(-100, 100);
var velocityY = 100;
var positionXp1 = 135;
var positionYp1 = 147;
var positionXp2 = 465;
var positionYp2 = 147;
var redpointnumberText;
var bluepointnumberText;
var setredpointnumberText;
var setbluepointnumberText;
var blueplayer = Phaser.Math.Between(0, 4);
var redplayer = Phaser.Math.Between(0, 4);


//game boarder
gameScene.lines = function(){
    var lineA = new Phaser.Geom.Line(91, 51, 299, 51);
    var lineB = new Phaser.Geom.Line(301, 51, 509, 51);
    var lineC = new Phaser.Geom.Line(91, 243, 299, 243);
    var lineD = new Phaser.Geom.Line(301, 243, 505, 243);
    var lineD = new Phaser.Geom.Line(301, 243, 509, 243);
    var lineE = new Phaser.Geom.Line(93, 51, 93, 243);
    var lineF = new Phaser.Geom.Line(507, 51, 507, 243);

    graphics = this.add.graphics(0,0); 

    graphics.lineStyle(4,0xFFFFFF,1);

    graphics.strokeLineShape(lineA);
    graphics.strokeLineShape(lineB);
    graphics.strokeLineShape(lineC);
    graphics.strokeLineShape(lineD);
    graphics.strokeLineShape(lineE);
    graphics.strokeLineShape(lineF);
  
}

//1st path
gameScene.curves = function(){
  elip = this.add.graphics();

  path = { t: 0, vec: new Phaser.Math.Vector2() };

  var startPoint = new Phaser.Math.Vector2(150, 55);
  var controlPoint1 = new Phaser.Math.Vector2(125, 100);
  var controlPoint2 = new Phaser.Math.Vector2(125, 190);
  var endPoint = new Phaser.Math.Vector2(150, 240);

  curve = new Phaser.Curves.CubicBezier(startPoint, controlPoint1, controlPoint2, endPoint);
}

//2nd path
gameScene.curvesB = function(){
  elip = this.add.graphics();

  path = { t: 0, vec: new Phaser.Math.Vector2() };

  var startPoint = new Phaser.Math.Vector2(450, 55);
  var controlPoint1 = new Phaser.Math.Vector2(475, 100);
  var controlPoint2 = new Phaser.Math.Vector2(475, 190);
  var endPoint = new Phaser.Math.Vector2(450, 240);

  curveB = new Phaser.Curves.CubicBezier(startPoint, controlPoint1, controlPoint2, endPoint);
}

// some parameters for our scene
gameScene.init = function() {

}

// load asset files for our game
gameScene.preload = function() {
  this.load.image('background', 'assets/images/court.png');
  this.load.image('red', 'assets/images/red.png');
  this.load.image('blue', 'assets/images/blue.png');
  this.load.image('ball', 'assets/images/ball.png');
  this.load.bitmapFont('orange', 'assets/fonts/font.png', 'assets/fonts/font.xml');
  this.load.bitmapFont('green', 'assets/fonts/fontgreen.png', 'assets/fonts/fontgreen.xml');
  this.load.bitmapFont('white', 'assets/fonts/fontwhite.png', 'assets/fonts/fontwhite.xml');
  this.load.bitmapFont('black', 'assets/fonts/fontblack.png', 'assets/fonts/fontblack.xml');
  this.load.image('delpotro', 'assets/images/delpotro.png');
  this.load.image('federer', 'assets/images/federer.png');
  this.load.image('kai', 'assets/images/kai.png');
  this.load.image('murray', 'assets/images/murray.png');
  this.load.image('nadal', 'assets/images/nadal.png');
  this.load.image('novak', 'assets/images/novak.png');
  this.load.image('thiem', 'assets/images/thiem.png');
  this.load.image('tsisipas', 'assets/images/tsisipas.png');
  this.load.image('wawrinka', 'assets/images/wawrinka.png');
  this.load.image('zverev', 'assets/images/zverev.png');

  this.load.spritesheet('delpotrocelebrate', 'assets/animations/delpotrocelebrate.png', { frameWidth: 480, frameHeight: 270 });
  this.load.spritesheet('nadalcelebrate', 'assets/animations/nadalcelebrate.png', { frameWidth: 480, frameHeight: 270 });
  this.load.spritesheet('tsisipascelebrate', 'assets/animations/tsisipascelebrate.png', { frameWidth: 480, frameHeight: 270 });
  this.load.spritesheet('zverevcelebrate', 'assets/animations/zverevcelebrate.png', { frameWidth: 480, frameHeight: 270 });

};

// executed once, after assets were loaded
gameScene.create = function(){
  //world bounds
  this.physics.world.setBounds(90, 48, 423, 198, true, true, true, true);

  //background
  this.background = this.add.sprite(300, 147, 'background');

  //animate del potro
  this.anims.create({
    key: 'celebrationDelPotro',
    frames: this.anims.generateFrameNames('delpotrocelebrate', {start: 1, end: 65}), 
    frameRate: 15,
    repeat: -1, 
   });

  //animate nadal
  this.anims.create({
    key: 'celebrationNadal',
    frames: this.anims.generateFrameNames('nadalcelebrate', {start: 1, end: 65}), 
    frameRate: 30,
    repeat: -1, 
   });

  //animate zverev
  this.anims.create({
    key: 'celebrationZverev',
    frames: this.anims.generateFrameNames('zverevcelebrate', {start: 1, end: 160}), 
    frameRate: 20,
    repeat: -1, 
   });

  //animate tsisipas
  this.anims.create({
    key: 'celebrationTsisipas',
    frames: this.anims.generateFrameNames('tsisipascelebrate', {start: 1, end: 175}), 
    frameRate: 30,
    repeat: -1, 
   });



  //player choosing
  if(redplayer == 0){
    playerred = this.add.sprite(75, 154, 'delpotro');
    playerred.setScale(1);
  }
  if(redplayer == 1){
    playerred = this.add.sprite(78, 156, 'nadal');
  }
  if(redplayer == 2){
    playerred = this.add.sprite(77, 145, 'zverev');
  }
  if(redplayer == 3){
    playerred = this.add.sprite(77, 149, 'murray');
  }
  if(redplayer == 4){
    playerred == this.add.sprite(87, 164, 'novak');
  }
  if(blueplayer == 0){
    playerblue = this.add.sprite(526.3, 146, 'federer');
    playerblue.setScale(0.97);
  }
  if(blueplayer == 1){
    playerblue = this.add.sprite(523, 146, 'kai');
  }
  if(blueplayer == 2){
    playerblue = this.add.sprite(523, 142, 'thiem');
  }
  if(blueplayer == 3){
    playerblue = this.add.sprite(525.5, 146.38, 'tsisipas');
    playerblue.setScale(0.97);
  }
  if(blueplayer == 4){
    playerblue = this.add.sprite(523, 158.88, 'wawrinka');
  }
  
  //bounds
  var graphics = this.add.graphics({ lineStyle: { width: 2, color: 0xFFFFFF }, fillStyle: { color: 0xFFFFFF } });
  var rect = new Phaser.Geom.Rectangle(95, 53);
    rect.width = 410;
    rect.height = 188;
    graphics.strokeRectShape(rect);
  
  //players
  this.player = this.physics.add.image(positionXp1, positionYp1, 'red').setImmovable().setCollideWorldBounds(true);
  this.player.setScale(0.8);
  this.player2 = this.physics.add.image(positionXp2, positionYp2, 'blue').setImmovable().setCollideWorldBounds(true);
  this.player2.setScale(0.8);
  
  //tennis ball
  this.ball = this.physics.add.image(301, this.sys.game.config.height / 2, 'ball').setCollideWorldBounds(true).setBounce(1);
  this.ball.setVelocityY(velocityY);
  this.ball.setVelocityX(velocityX);
  
  //point system
  this.redpointnumberText = this.add.bitmapText(240, 12, 'white', '15' ,30);
  this.bluepointnumberText = this.add.bitmapText(320, 12, 'white', '15' ,30);
  whitedashText = this.add.bitmapText(290, 10, 'white', '-' ,30);  
  this.setredpointnumberText = this.add.bitmapText(240, 252, 'white', '0' ,30);
  blackdashText = this.add.bitmapText(290, 252, 'white', '-' ,30);  
  this.setbluepointnumberText = this.add.bitmapText(325, 252, 'white', '0' ,30);

  //set colliders
  this.physics.add.collider(this.ball, this.player, this.hitPlayer, null, this);
  this.physics.add.collider(this.ball, this.player2, this.hitPlayer2, null, this);
  this.physics.add.collider(this.ball, this.player2, this.hitPlayer2, null, this);

  //function calls
  this.curves();
  this.curvesB();

  //key inputs
  keyW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
  keyS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
  keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  cursors = this.input.keyboard.createCursorKeys();

};


gameScene.update = function(){
  //curved paths
  elip.clear();

  this.redpointnumberText.setText(pointred);
  this.bluepointnumberText.setText(pointblue);
  this.setredpointnumberText.setText(setred);
  this.setbluepointnumberText.setText(setblue);

  //advantage
  if(pointred == advantagered && pointblue == 40){
    if(this.ball.x < 107){
      pointred = 40;
      pointblue = 40;
    }
    if(this.ball.x > 496){
      velocityX = -velocityX;
      pointred = 41;
    }
  }

  //advantage
  if(pointblue == advantageblue && pointred == 40){
    if(this.ball.x < 107){
      velocityX = -velocityX;
      pointblue = 41;
    }
    if(this.ball.x > 496){
      pointblue = 40;
      pointred = 40;
    }
  }

  //advantage
  if(pointblue == 40 && pointred == 40){
    if(this.ball.x < 107){
      pointblue = advantageblue;
    }
    if(this.ball.x > 496){
      pointred = advantagered;
    }
  }

  //points blue
  if(this.ball.x < 107){
    if(pointblue == 40){
      velocityX = -velocityX;
      pointblue = 41;
    }
    if(pointblue == 30){
      pointblue = 40;
    }
    if(pointblue == 15){
      pointblue = 30;
    }
    if(pointblue == 0){
      pointblue = 15;
    }
    this.ball.x = 301;
    this.ball.y = this.sys.game.config.height / 2;

    this.ball.setVelocityX(velocityX);
  }

  //point red
  if(this.ball.x > 496){
    if(pointred == 40){
      velocityX = -velocityX;
      pointred = 41;
    }
    if(pointred == 30){
      pointred = 40;
    }
    if(pointred == 15){
      pointred = 30;
    }
    if(pointred == 0){
      pointred = 15;
    }
    this.ball.x = 301;
    this.ball.y = this.sys.game.config.height / 2;

    this.ball.setVelocityX(velocityX);
  }

  //sets
  if(pointred == 41){
    pointred = 0;
    pointblue = 0;
    setred = setred + 1;
  }
  if(pointblue == 41){
    pointblue = 0;
    pointred = 0;
    setblue = setblue + 1;
  }

  //boundary
  if(this.player.x > 291){
    this.player.x = 290;
  }
  if(this.player2.x < 309.25){
    this.player2.x = 310.25;
  }
  if(this.player2.x < 309.25){
    this.player2.x = 310.25;
  }

  //key inputs
  if(keyW.isDown){
    this.player.y = this.player.y - 2;
    console.log("W");
  }
  if(keyS.isDown){
    this.player.y = this.player.y + 2;
    console.log("S");
  }
  if(keyA.isDown){
    this.player.x = this.player.x - 2;
    console.log("A");
  }
  if(keyD.isDown){
    this.player.x = this.player.x + 2;
    console.log("D");
  }
  if (cursors.up.isDown){
    this.player2.y = this.player2.y - 2;
    console.log("up");
  }
  if (cursors.down.isDown){
    this.player2.y = this.player2.y + 2;
    console.log("down");
  }
  if (cursors.right.isDown){
    this.player2.x = this.player2.x + 2;
    console.log("right");
  } 
  if (cursors.left.isDown){
    this.player2.x = this.player2.x - 2;
    console.log("left");
  }

  //celebrations
  if(redplayer == 0){
    if(setred == 6){
      delpotrocelebrationgif = this.add.sprite(300, 165, 'delpotrocelebrate', 0).play('celebrationDelPotro');
      delpotrocelebrationgif.setScale(1.25);
      setred = 7;
      this.ball.destroy();
    }
  }
  if(redplayer == 1){
    if(setred == 6){
      nadalcelebrationgif = this.add.sprite(300, 165, 'nadalcelebrate', 0).play('celebrationNadal');
      nadalcelebrationgif.setScale(1.25);
      setred = 7;
      this.ball.destroy();
    }
  }
  if(redplayer == 2){
    if(setred == 6){
      zverevcelebrationgif = this.add.sprite(300, 165, 'zverevcelebrate', 0).play('celebrationZverev');
      zverevcelebrationgif.setScale(1.25);
      setred = 7;
      this.ball.destroy();
    }
  }
  if(blueplayer == 3){
    if(setblue == 6){
      tsisipascelebrationgif = this.add.sprite(300, 160, 'tsisipascelebrate', 0).play('celebrationTsisipas');
      tsisipascelebrationgif.setScale(1.25);
      setblue = 7;
      this.ball.destroy();
    }
  }
};

// our game's configuration
let config = {
  type: Phaser.AUTO,
  width: 600,
  height: 294,
  physics: {
    default: 'arcade'
    },
  scene: gameScene

};

// create the game, and pass it the configuration
let game = new Phaser.Game(config);